﻿using ByteSizeLib;

namespace altium.core.Models.Monitors
{
    public class MemoryUsage
    {
        public long MemoryMin { get; set; }
        public long MemoryMax { get; set; }
        public double MemoryAvg { get; set; }
        public long ValuesCount { get; set; }

        public override string ToString() =>
            $"Memory: " +
            $"min({ByteSize.FromBytes(MemoryMin).ToBinaryString()}), " +
            $"max({ByteSize.FromBytes(MemoryMax).ToBinaryString()}), " +
            $"avg({ByteSize.FromBytes(MemoryAvg).ToBinaryString()}), " +
            $"count({ValuesCount})";
    }
}
