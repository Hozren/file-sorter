﻿namespace altium.core.Models.Monitors
{
    public class TimeUsage
    {
        public DateTime Started { get; set; }
        public DateTime Ended { get; set; }
        public double TotalMilliseconds => (Ended - Started).TotalMilliseconds;

        public override string ToString() =>
            $"Time: started('{Started}'), ended('{Ended}'), total milliseconds('{TotalMilliseconds}')";
    }
}
