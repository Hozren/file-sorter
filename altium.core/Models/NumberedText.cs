﻿namespace altium.core.Models
{
    public class NumberedText : IComparable<NumberedText>
    {
        public long Number { get; set; }
        public string Text { get; set; }

        public NumberedText(long number, string text)
        {
            Number = number;
            Text = text;
        }

        public static NumberedText? Parse(string? line)
        {
            if (line == null) return null;
            var parts = line.Split(". ");
            long.TryParse(parts.First(), out var number);
            return new NumberedText(number, parts.Last());
        }

        public override string ToString() => $"{Number}. {Text}";

        public int CompareTo(NumberedText? other)
        {
            var compareText = Text.CompareTo(other?.Text);
            if (compareText != 0) return compareText;

            var compareNumber = Number.CompareTo(other.Number);
            return compareNumber;
        }
    }
}
