﻿namespace altium.core.Monitors
{
    public interface IAppMonitor<TOutput>
    {
        void Start();
        void Stop();
        TOutput GetResult();
    }

    public static class AppMonitors
    {
        public static object[] RunWithMonitoring(Action action)
        {
            var timeMonitor = new TimeMonitor();
            var memMonitor = new MemoryMonitor();
            timeMonitor.Start();
            memMonitor.Start();

            action();

            timeMonitor.Stop();
            memMonitor.Stop();

            return new object[] { timeMonitor.GetResult(), memMonitor.GetResult() };
        }
    }
}
