﻿using altium.core.Models.Monitors;
using System.Diagnostics;

namespace altium.core.Monitors
{
    public class MemoryMonitor : IAppMonitor<MemoryUsage>
    {
        private MemoryUsage _data;
        private List<long> _memoryUsage;
        private bool _isRunning = false;

        public MemoryMonitor()
        {
            _data = new MemoryUsage();
            _memoryUsage = new List<long>();
        }

        public MemoryUsage GetResult()
        {
            return _data;
        }

        public async void Start()
        {
            _isRunning = true;
            var proc = Process.GetCurrentProcess();

            while (_isRunning)
            {
                _memoryUsage.Add(proc.PrivateMemorySize64);
                proc.Refresh();
                await Task.Delay(1000);
            }
        }

        public void Stop()
        {
            _isRunning = false;
            _data.MemoryMin = _memoryUsage.Min();
            _data.MemoryMax = _memoryUsage.Max();
            _data.MemoryAvg = _memoryUsage.Average();
            _data.ValuesCount = _memoryUsage.Count;
        }
    }
}
