﻿using altium.core.Models.Monitors;

namespace altium.core.Monitors
{
    public class TimeMonitor : IAppMonitor<TimeUsage>
    {
        private TimeUsage _data;

        public TimeMonitor()
        {
            _data = new TimeUsage();
        }

        public TimeUsage GetResult()
        {
            return _data;
        }

        public void Start()
        {
            _data.Started = DateTime.UtcNow;
        }

        public void Stop()
        {
            _data.Ended = DateTime.UtcNow;
        }
    }
}
