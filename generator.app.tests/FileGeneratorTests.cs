﻿using ByteSizeLib;
using generator.app.Generators;
using generator.app.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace generator.app.tests
{
    // variation of workers
    [TestFixture(10, "100MB", 1)]
    [TestFixture(10, "100MB", 2)]
    [TestFixture(10, "100MB", null)]

    // variation of duplicate ratio
    [TestFixture(10, "100MB", 1)]
    [TestFixture(20, "100MB", 2)]
    [TestFixture(30, "100MB", null)]
    [TestFixture(90, "100MB", null)]

    // big file
    //[TestFixture(10, "1GB", 20)]
    public class FileGeneratorTests
    {
        private GeneratorOptions _options;
        private FileGenerator _generator;
        private FileInfo _outputFile;

        public FileGeneratorTests(int duplicateRatio, string size, int? workersCount)
        {
            _outputFile = new("output_test.txt" + Random.Shared.Next());
            _options = new GeneratorOptions(_outputFile, duplicateRatio, size, workersCount);
            _generator = new FileGenerator(_options);
        }

        [SetUp]
        public void Setup()
        {
            Assert.DoesNotThrowAsync(async () =>
            {
                await _generator.GenerateAsync();
            });
        }

        [Test]
        public void check_size_limitation_test()
        {
            var actualFileSize = ByteSize.FromBytes(_outputFile.Length);
            Assert.That(actualFileSize, Is.GreaterThan(_options.Size));
            Assert.That(_options.Size.AddMegaBytes(100), Is.GreaterThan(actualFileSize));
        }

        [Test]
        public void check_word_duplications_test()
        {
            var wordsCounter = new Dictionary<string, long>();
            var allLinesCount = 0;

            using (var s = _outputFile.OpenRead())
            {
                using var f = new StreamReader(s);
                while (!f.EndOfStream)
                {
                    var line = f.ReadLine();
                    Assert.IsNotEmpty(line);
                    allLinesCount++;
                    var text = line.Split(' ').Last();

                    Assert.IsNotEmpty(text);
                    if (!wordsCounter.ContainsKey(text))
                    {
                        wordsCounter.Add(text, 0);
                    }
                    wordsCounter[text]++;
                }
            }
            var isExistsDuplications = wordsCounter.Any(x => x.Value > 1);
            TestContext.WriteLine($"duplication Exists: {isExistsDuplications}");
            Assert.That(isExistsDuplications);

            var duplicateRatioNormalized = (double)_options.DuplicateRatio / 100;
            var duplcatesCount = wordsCounter.Count(x => x.Value > 1);
            var actualDuplicatesRatio = (double)duplcatesCount / allLinesCount;
            TestContext.WriteLine($"all lines count: {allLinesCount}");
            TestContext.WriteLine($"duplcates count: {duplcatesCount}");
            TestContext.WriteLine($"expected duplicates rate: {duplicateRatioNormalized}");
            TestContext.WriteLine($"actual duplicates ratio: {actualDuplicatesRatio}");
        }
    }
}