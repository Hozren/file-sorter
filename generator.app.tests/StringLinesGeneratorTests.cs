using generator.app.Generators;
using NUnit.Framework;

namespace generator.app.tests
{
    public class NumberedTextGeneratorTest
    {
        private NumberedTextGenerator _generator;

        public NumberedTextGeneratorTest()
        {
            _generator = new NumberedTextGenerator();
        }

        [Test]
        public void generated_lines_are_different()
        {
            var word1 = _generator.NextWord();
            Assert.IsNotEmpty(word1.Text);
            Assert.AreNotEqual(word1.Number, default);
            Assert.AreEqual(word1.Text, word1.Text.Trim());

            var word2 = _generator.NextWord();
            Assert.IsNotEmpty(word2.Text);
            Assert.AreNotEqual(word2.Number, default);
            Assert.AreEqual(word2.Text, word2.Text.Trim());

            Assert.AreNotEqual(word1.ToString(), word2.ToString());
            Assert.AreNotEqual(word1.Number, word2.Number);
        }
    }
}