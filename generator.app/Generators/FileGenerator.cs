﻿using altium.core.Models;
using generator.app.Models;
using System.Collections.Concurrent;

namespace generator.app.Generators
{
    public class FileGenerator
    {
        private readonly GeneratorOptions _generatorOptions;
        private readonly ConcurrentQueue<string> _duplicationWordsQueue;
        private readonly List<FileGeneratorWorker> _workers;
        private readonly Random _random;

        private bool _isMerging = false;
        private object _lock = new();
        private Task _mergingTask;

        public FileGenerator(GeneratorOptions generatorOptions)
        {
            _generatorOptions = generatorOptions;
            _duplicationWordsQueue = new ConcurrentQueue<string>();
            _workers = new List<FileGeneratorWorker>(_generatorOptions.WorkersCount);
            _random = Random.Shared;
        }

        public async Task GenerateAsync()
        {
            PrepeareWorkers();
            var tasks = new List<Task>();
            foreach (var worker in _workers)
            {
                tasks.Add(worker.Run());
            }
            await Task.WhenAll(tasks);
            if (_mergingTask != null) await _mergingTask;
        }

        private void PrepeareWorkers()
        {
            var fileSizeForWorker = ((long)_generatorOptions.Size.Bytes) / _generatorOptions.WorkersCount;
            for (int i = 0; i < _generatorOptions.WorkersCount; i++)
            {
                var worker = new FileGeneratorWorker(_generatorOptions.Output.FullName, fileSizeForWorker);
                worker.NumberedTextGenerated += OnNumberedTextGeneratedHandler;
                worker.GenerationDone += Worker_GenerationDone;
                _workers.Add(worker);
            }
        }

        private void Worker_GenerationDone(FileGeneratorWorker worker)
        {
            _mergingTask ??= Task.Run(MergeFilesAsync);
        }

        private async Task MergeFilesAsync()
        {
            if (_isMerging) return;

            lock (_lock)
            {
                _isMerging = true;
            }

            using var destStream = _generatorOptions.Output.Open(FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
            while (_workers.Any(x => !x.IsResultHandled))
            {
                var worker = _workers.FirstOrDefault(x => !x.IsResultHandled && !x.IsRunning);
                if (worker == null)
                {
                    continue;
                }
                var filename = worker.ResultFile.FullName;

                using (var srcStream = File.OpenRead(filename))
                {
                    await srcStream.CopyToAsync(destStream);
                }
                worker.MarkAsHandled();


            }

            lock (_lock)
            {
                _isMerging = false;
            }
        }

        private void OnNumberedTextGeneratedHandler(NumberedText numberedText)
        {
            HandleDuplication(numberedText);
        }

        private void HandleDuplication(NumberedText numberedText)
        {
            if (IsTimeToMakeDuplicate() && _duplicationWordsQueue.TryDequeue(out var word))
            {
                numberedText.Text = word;
                return;
            }

            if (IsTimeToMakeDuplicate())
            {
                _duplicationWordsQueue.Enqueue(numberedText.Text);
            }
        }

        private bool IsTimeToMakeDuplicate() => _random.Next(100) < _generatorOptions.DuplicateRatio;
    }
}
