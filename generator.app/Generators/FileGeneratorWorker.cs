﻿using altium.core.Models;

namespace generator.app.Generators
{
    public class FileGeneratorWorker
    {
        private readonly NumberedTextGenerator _generator;
        private readonly long _expectedFileSize;
        private readonly FileInfo _tmpFile;

        public bool IsRunning { get; private set; }
        public bool IsResultHandled { get; private set; } = false;
        public FileInfo ResultFile => _tmpFile;

        public event Action<NumberedText> NumberedTextGenerated = delegate { };
        public event Action<FileGeneratorWorker> GenerationDone = delegate { };

        public FileGeneratorWorker(string targetFileName, long fileSize)
        {
            _generator = new NumberedTextGenerator();
            _tmpFile = new FileInfo(targetFileName + GetHashCode());
            _expectedFileSize = fileSize;
        }

        public Task Run()
        {
            IsRunning = true;
            return Task.Run(GenerateLinesAsync);
        }

        public void MarkAsHandled()
        {
            IsResultHandled = true;
            _tmpFile.Delete();
        }

        private async Task GenerateLinesAsync()
        {
            var f = new FileStream(_tmpFile.FullName, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            using var s = new StreamWriter(f);
            s.AutoFlush = true;

            while (f.Length < _expectedFileSize)
            {
                var line = _generator.NextWord();
                NumberedTextGenerated(line);
                await s.WriteLineAsync(line.ToString());
            }
            s.Close();
            f.Close();

            GenerationDone(this);
            IsRunning = false;
        }
    }
}
