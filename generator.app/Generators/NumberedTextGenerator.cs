﻿using altium.core.Models;
using System.Text;

namespace generator.app.Generators
{
    public class NumberedTextGenerator
    {
        private const int WORD_MIN_LENGTH = 5;
        private const int WORD_MAX_LENGTH = 42;
        private readonly static char[] SPECIAL_SYMBOLS = new[] { ' ' };

        private int _lineMaxLength =
            WORD_MAX_LENGTH +
            WORD_MIN_LENGTH +
            SPECIAL_SYMBOLS.Length;

        private readonly char[] _availableChars;
        private readonly Random _random;

        public NumberedTextGenerator()
        {
            _availableChars = (new[] {
                GetRangeOfSymbols('A', 'Z'),
                GetRangeOfSymbols('a', 'z'),
                GetRangeOfSymbols('0', '9'),
                SPECIAL_SYMBOLS
            })
            .SelectMany(x => x)
            .ToArray();

            _random = Random.Shared;
        }

        public NumberedText NextWord()
        {
            var newWordBuilder = new StringBuilder(WORD_MIN_LENGTH, _lineMaxLength);
            var wordLength = _random.Next(WORD_MIN_LENGTH, WORD_MAX_LENGTH);
            for (int i = 0; i < wordLength; ++i)
            {
                newWordBuilder.Append(NextChar());
            }
            return new NumberedText(_random.Next(), newWordBuilder.ToString().Trim());
        }

        private char NextChar() => _availableChars[_random.Next(_availableChars.Length)];

        private static IEnumerable<char> GetRangeOfSymbols(char start, char end) =>
            Enumerable
            .Range(start, end - start + 1)
            .Select(x => (char)x);
    }
}
