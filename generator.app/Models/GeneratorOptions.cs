﻿using ByteSizeLib;

namespace generator.app.Models
{
    public class GeneratorOptions
    {
        private const string DEFAULT_OUTPUT_FILE_NAME = "output.txt";
        private const int DEFAULT_DUPLICATE_RATIO = 10;
        private const int DEFAULT_OUTPUT_FILE_SIZE_MB = 10;

        public GeneratorOptions(FileInfo? output = null, int? duplicateRatio = null, string size = null, int? workersCount = null)
        {
            Output = output ?? new FileInfo(DEFAULT_OUTPUT_FILE_NAME);
            DuplicateRatio = duplicateRatio ?? DEFAULT_DUPLICATE_RATIO;
            if (DuplicateRatio < 0 || DuplicateRatio > 100)
            {
                throw new ArgumentException("duplicate ratio must be in [0..100]");
            }

            Size = ByteSize.TryParse(size, out var parsed)
                ? parsed
                : ByteSize.FromMebiBytes(DEFAULT_OUTPUT_FILE_SIZE_MB);

            WorkersCount = workersCount ?? Environment.ProcessorCount;
            if (WorkersCount < 1 || WorkersCount > 100)
            {
                throw new ArgumentException("workers count must be in [1..100]");
            }
        }

        public FileInfo Output { get; set; }

        public int DuplicateRatio { get; set; }

        public ByteSize Size { get; set; }

        public int WorkersCount { get; set; }

        public override string ToString()
        {
            return
                $"File: '{Output.FullName}' " +
                $"Ratio: '{DuplicateRatio}/100' " +
                $"Workers: '{WorkersCount}' " +
                $"Size: '{Size.ToBinaryString()}'";
        }
    }
}
