﻿using altium.core.Monitors;
using generator.app.Generators;
using generator.app.Models;

class Program
{
    /// <summary>
    /// Generator of text file with collection samples like 
    /// 415. Apple
    /// 30432. Something something something
    /// 1. Apple
    /// 32. Cherry is the best
    /// 2. Banana is yellow
    /// ...
    /// 
    /// with specified file size and ratio of duplication
    /// </summary>
    /// <param name="output">Path to output file</param>
    /// <param name="duplicateRatio">Ratio of word duplication to whole collection in percent. 0 - no duplications, 100 - all duplicated</param>
    /// <param name="size">Size of generated file (example: 50KB, 100MB, 2GB, 1TB...)</param>
    /// <param name="workersCount">Count of workers</param>
    public static void Main(FileInfo output, int? duplicateRatio, string size, int? workersCount)
    {
        var args = new GeneratorOptions(output, duplicateRatio, size, workersCount);
        Console.WriteLine(args.ToString());
        var generator = new FileGenerator(args);

        var usage = AppMonitors.RunWithMonitoring(() => generator.GenerateAsync().Wait());
        foreach (var u in usage) Console.WriteLine(u.ToString());
    }
}