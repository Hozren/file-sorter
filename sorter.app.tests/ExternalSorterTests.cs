﻿using NUnit.Framework;
using sorter.app.Sorter;
using System.IO;
using System.Threading.Tasks;

namespace sorter.app.tests
{
    public class ExternalSorterTests
    {
        [TestCase("samples/short_sample.txt")]
        [TestCase("samples/1KiB_sample.txt")]
        [TestCase("samples/1MiB_sample.txt")]
        [TestCase("samples/10MiB_sample.txt")]
        //[TestCase("samples/100MiB_sample.txt")]
        public async Task sorting_test(string filename)
        {
            var source = new FileInfo(filename);
            var options = new SorterOptions(source);
            var sorter = new ExternalSorter(options);

            await sorter.SortAsync();

            Assert.That(options.OutputFile.Length, Is.EqualTo(source.Length));
        }
    }
}
