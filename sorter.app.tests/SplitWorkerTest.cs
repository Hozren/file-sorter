using NUnit.Framework;
using sorter.app.Sorter;
using System.IO;
using System.Threading.Tasks;

namespace sorter.app.tests
{
    public class SplitWorkerTest
    {
        const string SHORT_SAMPLE = @"samples/short_sample.txt";
        private FileInfo _file;

        [SetUp]
        public void Setup()
        {
            _file = new FileInfo(SHORT_SAMPLE);
        }

        [Test]
        [TestCase(0, 154,
@"100. aaaaaaaaaaaa aaaaaaaaaa
50. bbbbbbbbbbbbbbbbbbb
75. ccccccccc ccccccccccc
150. dddddddd.dddd.dddddddd
25. eeeeeeeee eeeeeeeee
35. wwwwwwwwwwwww
")]
        [TestCase(42, 81,
@"50. bbbbbbbbbbbbbbbbbbb
150. dddddddd.dddd.dddddddd
25. eeeeeeeee eeeeeeeee
")]
        [TestCase(123, 50,
@"35. wwwwwwwwwwwww
")]
        [TestCase(0, 25,
@"75. ccccccccc ccccccccccc
")]
        [TestCase(25, 53,
@"100. aaaaaaaaaaaa aaaaaaaaaa
25. eeeeeeeee eeeeeeeee
")]
        [TestCase(0, 26,
@"75. ccccccccc ccccccccccc
")]
        [TestCase(26, 53,
@"100. aaaaaaaaaaaa aaaaaaaaaa
25. eeeeeeeee eeeeeeeee
")]
        [TestCase(0, 27,
@"100. aaaaaaaaaaaa aaaaaaaaaa
75. ccccccccc ccccccccccc
")]
        [TestCase(27, 53,
@"25. eeeeeeeee eeeeeeeee
")]
        [TestCase(0, 28,
@"100. aaaaaaaaaaaa aaaaaaaaaa
75. ccccccccc ccccccccccc
")]
        [TestCase(28, 53,
@"25. eeeeeeeee eeeeeeeee
")]
        public async Task split_worker_test(long offset, long length, string expectedText)
        {
            var options = new SorterOptions(_file, null, 1);

            var splitWorker = new SplitWorker(options, offset, length);

            await splitWorker.RunSortAsync();

            var actualContent = File.ReadAllText(splitWorker.ResultFile.FullName);

            Assert.AreEqual(expectedText, actualContent);
        }
    }
}