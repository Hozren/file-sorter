﻿using altium.core.Monitors;
using sorter.app.Sorter;

class Program
{
    /// <summary>
    /// Application for sorting big text files with content like:
    /// 415. Apple
    /// 30432. Something something something
    /// 1. Apple
    /// 32. Cherry is the best
    /// 2. Banana is yellow
    /// ...
    /// 
    /// Sorting by text, then by number
    /// </summary>
    /// <param name="source">Path to input file</param>
    /// <param name="output">Path file with sorted text</param>
    /// <param name="workerCount">Count of maximum workers</param>
    /// <returns></returns>
    public static void Main(FileInfo source, FileInfo output, int? workerCount)
    {
        var options = new SorterOptions(source, output, workerCount);
        Console.WriteLine(options);
        var sorter = new ExternalSorter(options);
        var usage = AppMonitors.RunWithMonitoring(() => sorter.SortAsync().Wait());
        foreach (var u in usage) Console.WriteLine(u.ToString());
    }
}