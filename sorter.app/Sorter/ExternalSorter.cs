﻿using altium.core.Models;
using ByteSizeLib;

namespace sorter.app.Sorter
{
    public class ExternalSorter
    {
        private SorterOptions _options;
        private List<SplitWorker> _workers;
        private readonly double TEMP_SIZE = ByteSize.FromMebiBytes(50).Bytes;

        public ExternalSorter(SorterOptions options)
        {
            _options = options;
        }

        public async Task SortAsync()
        {
            await SplitAsync();
            await Merge();
        }

        private async Task SplitAsync()
        {
            PrepareWorkers();
            await RunWorkersAsync();
        }

        private async Task RunWorkersAsync()
        {
            while (_workers.Where(w => !w.IsCompleted).Any())
            {
                var tasks = new List<Task>();
                var runned = _workers.Where(w => !w.IsCompleted && w.IsRunning).ToList();
                var runnedCount = runned.Count;
                if (runnedCount < _options.WorkersCount)
                {
                    var nextWorkers = _workers.Where(w => !w.IsCompleted && !w.IsRunning);
                    runned.AddRange(nextWorkers);
                    tasks = tasks.Where(x => !x.IsCompleted).ToList();
                    tasks.AddRange(nextWorkers.Select(async w => await w.RunSortAsync()));
                }
                if (tasks.Any())
                {
                    await Task.WhenAny(tasks);
                }
            }
        }

        private void PrepareWorkers()
        {
            var totalSize = _options.SourceFile.Length;
            var workerCount = Math.Max((int)Math.Ceiling(totalSize / TEMP_SIZE), _options.WorkersCount);
            _workers = new List<SplitWorker>(workerCount);
            var workerLength = Math.Min((long)TEMP_SIZE, totalSize / workerCount);
            long offset = 0;

            for (int i = 0; i < workerCount; i++, offset += workerLength + 1)
            {
                var worker = new SplitWorker(_options, offset, workerLength);
                _workers.Add(worker);
            }
        }

        private async Task Merge()
        {
            var merger = new Merger(_workers.Select(x => x.ResultFile), _options.OutputFile);
            await merger.RunAsync();
        }
    }

    public class SplitWorker
    {
        public bool IsRunning { get; private set; }
        public bool IsCompleted { get; private set; }
        public FileInfo ResultFile { get; private set; }

        private readonly SorterOptions _options;
        private readonly long _offset;
        private readonly long _length;

        private List<NumberedText> _sortedLines;


        public SplitWorker(SorterOptions options, long offset, long length)
        {
            _options = options;
            ResultFile = new FileInfo(options.SourceFile.FullName + this.GetHashCode());
            _offset = offset;
            _length = length;
            _sortedLines = new();
            IsCompleted = false;
            IsRunning = false;
        }

        public async Task RunSortAsync()
        {
            IsRunning = true;
            using (var s = _options.SourceFile.OpenRead())
            {
                var f = new StreamReader(s);
                s.Position = s.Seek(_offset, SeekOrigin.Begin);
                var endPosition = _offset + s.Length;
                var readedCount = 0;

                // skip until next line end
                char? character = null;
                while (_offset != 0 && character != '\n')
                {
                    character = (char)f.Read();
                    readedCount++;
                }

                while (readedCount <= _length)
                {
                    // read full line
                    var line = await f.ReadLineAsync();

                    if (line == null) break;
                    readedCount += line.Length + Environment.NewLine.Length;

                    _sortedLines.Add(NumberedText.Parse(line));
                }
                _sortedLines.Sort();
                await File.WriteAllLinesAsync(ResultFile.FullName, _sortedLines.Select(x => x.ToString()));
            }
            Done();
        }

        private void Done()
        {
            _sortedLines = null;
            GC.Collect();
            IsCompleted = true;
            IsRunning = false;
        }
    }
}
