﻿using altium.core.Models;

namespace sorter.app.Sorter
{
    public class Merger
    {
        private IEnumerable<FileInfo> _sources;
        private FileInfo _outputFile;

        public Merger(IEnumerable<FileInfo> sources, FileInfo output)
        {
            _sources = sources;
            _outputFile = output;
        }

        public async Task RunAsync()
        {
            using var outputStream = new StreamWriter(_outputFile.OpenWrite());

            var readers = _sources
                .Select(x => new LineReader(x))
                .ToList();
            await Task.WhenAll(readers.Select(async x => await x.ReadNextAsync()));

            readers.RemoveAll(x => x.Line == null);

            while (true)
            {
                readers.Sort((a, b) => a.Line.CompareTo(b.Line));
                var top = readers.FirstOrDefault();
                if (top?.Line == null) break;
                await outputStream.WriteLineAsync(top.Line.ToString());
                await top.ReadNextAsync();
                if (top.Line == null) readers.Remove(top);
            }
        }

        class LineReader
        {
            private readonly FileInfo _file;
            private readonly StreamReader _stream;

            public NumberedText? Line { get; private set; }

            public LineReader(FileInfo file)
            {
                _file = file;
                _stream = file.OpenText();
            }

            public async Task ReadNextAsync()
            {
                if (_stream == null) return;

                if (_stream.EndOfStream)
                {
                    Line = null;
                    _stream.Close();
                    _file.Delete();
                    return;
                }
                Line = NumberedText.Parse(await _stream.ReadLineAsync());
            }
        }
    }
}
