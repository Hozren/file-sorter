﻿namespace sorter.app.Sorter
{
    public class SorterOptions
    {
        public FileInfo SourceFile { get; private set; }
        public FileInfo OutputFile { get; private set; }
        public int WorkersCount { get; private set; }

        public SorterOptions(FileInfo source, FileInfo output = null, int? workerCount = null)
        {
            if (source == null || !source.Exists)
            {
                throw new ArgumentException("Unable to read source file");
            }
            SourceFile = source;
            OutputFile = output ?? new FileInfo(SourceFile.FullName + ".sorted");
            WorkersCount = workerCount ?? Environment.ProcessorCount;
        }

        public override string ToString() => $"file: '{SourceFile.FullName}', output: '{OutputFile.FullName}' workers count: '{WorkersCount}'";
    }
}
